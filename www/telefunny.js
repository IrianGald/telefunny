/*
 * Objeto telefunny personalizado para obtener y almacenar los valores de la información de la telefonía retornada del código nativo.
 * Propiedades incluídas:
 * 1. IMEI
 * 2. Operator Name
 * 3. CellID latched to
 * 4. LAC of the current CellID
 * 5. Neighboring Cell Sites
 */

var exec = require('cordova/exec'), cordova = require('cordova');
//cordova.exec(successCallback, failureCallback, service, action, [args]);
//cordova.exec(successCallback, failureCallback, service, action, [args]);

var Telefunny = function() {
    this.imei = null;
    this.operator = null;
    this.cellID = null;
    this.lac = null;
    this.neighbors = {};
    // Crea nuevos event handlers en window (retorna una instancia channel)
    this.channels = {
        watchingnetwork: cordova.addWindowEventHandler("watchingnetwork")
    };
    for (var key in this.channels) {
        this.channels[key].onHasSubscribersChange = Telefunny.onHasSubscribersChange;
    }

};

Telefunny.onHasSubscribersChange = function() {
    exec(telefunny.status, telefunny.error, "Telefunny", "getTelefunnyInfo", []);
}

/**
 * Callback para telefunny inicializado
 * @param {Object} info            keys: imei, isPlugged
 */
Telefunny.prototype.status = function(info) {
    cordova.fireWindowEvent("watchingnetwork", info);
    if (info) {
        if (telefunny.imei !== info.imei || telefunny.operator !== info.operator) {

            if (info.imei == null && telefunny.imei != null) {
                return; // Caso especial donde el callback es llamado porque hemos detenido el listening al código nativo.
            }

            // Algo cambió. Dispara el evento watching network
            telefunny.imei = info.imei;
            telefunny.operator = info.operator;
            telefunny.cellID = info.cellID;
            telefunny.lac = info.lac;
            telefunny.neighbors = info.neighbors;
        }
    }
};

/**
 * Callback de error para telefunny inicializado
 */
Telefunny.prototype.error = function(e) {
    console.log("Error inicializando el plugin de información de red telefónica: " + e);
};

var telefunny = new Telefunny();

module.exports = telefunny;
