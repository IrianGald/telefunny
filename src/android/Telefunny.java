package com.bitworks.cordova.telefunny;

import java.util.List;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

public class Telefunny extends CordovaPlugin {

	TelephonyManager Tel;
	GsmCellLocation cellLocation;
	MyPhoneStateListener MyListener;
	public static String imei;
	public static String operator;
	private static List<NeighboringCellInfo> NeighboringList;
	public static int cellID;
	public static int lac;

	public Telefunny() {}

	/**
     * Configura el context del Command. Estonces esto puede ser usado para hacer cosas como
     * obtener las rutas de archivos asociados con la Activity.
	 *
	 * @param cordova
     *            El contexto de la Activity princiapl.
	 * @param webView
     *            El CordovaWebView de Cordova está en ejecución.
	 */
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
        /*
         * Obtiene el servicio de la telefonía desde el código navito, el contexto será la aplicación cordova
         * donde se invoca el servicio.
         */
		this.Tel = (TelephonyManager) cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
		this.cellLocation = (GsmCellLocation) Tel.getCellLocation();
	}

	/**
     * Ejecuta la solicitud y retorna el resultado del plugin.
     *
     * @param action            La acción a ejecutar.
     * @param args              JSONArray de los argumentos del plugin.
     * @param callbackContext   El callback id usado cuando se llama de regreso al JavaScript.
     * @return                  True si la acción fue válida, sino es false.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getTelefunnyInfo")) {
            JSONObject r = new JSONObject();
            r.put("imei", this.getMobileIdentifier());
            r.put("operator", this.getOperator());
            r.put("cellID", this.getCellID());
            r.put("lac", this.getLac());
            r.put("neighbors", this.getNeighbours());
            callbackContext.success(r);
        }
        else {
            return false;
        }
        return true;
    }

    /*
     * Todos los métodos locales para responder a la aplicación cordova.
     * 1. getMobileIdentifier()
     * 2. getOperator()
     * 3. getCellID()
     * 4. getLac()
     * 5. getNeighbours()
     */

    public String getMobileIdentifier(){
    	return this.Tel.getDeviceId();
    }

    public String getOperator(){
    	return this.Tel.getNetworkOperatorName();
    }

    public int getCellID(){
    	return this.cellLocation.getCid();
    }

    public int getLac(){
    	return this.cellLocation.getLac();
    }

    public JSONArray getNeighbours(){
		this.setNeighboringList(this.Tel.getNeighboringCellInfo());
    	JSONArray neighborsArray = new JSONArray();
    	for(int i=0; i< getNeighboringList().size(); i++){

            JSONObject neighbors = new JSONObject();
    		String dbM = null, cid = null, lac = null;

    		cid = String.valueOf(getNeighboringList().get(i).getCid());
    		lac = String.valueOf(getNeighboringList().get(i).getLac());
    		int rssi = getNeighboringList().get(i).getRssi();

    		if(rssi == NeighboringCellInfo.UNKNOWN_RSSI)
    			dbM = "Unknown RSSI";
    		else
    			dbM = String.valueOf(-113 + 2 * rssi) + " dBm";
    		try {
				neighbors.put("rssi", dbM);
				neighbors.put("cid", cid);
	    		neighbors.put("lac", lac);
	    		neighborsArray.put(neighbors);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

    	}
    	return neighborsArray;
    }


	public static List<NeighboringCellInfo> getNeighboringList() {
		return NeighboringList;
	}

	public void setNeighboringList(List<NeighboringCellInfo> neighboringList) {
		NeighboringList = neighboringList;
	}


	private class MyPhoneStateListener extends PhoneStateListener {}
}
